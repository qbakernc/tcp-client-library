﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisleTCPLibrary
{
    /// <summary>
    /// TCPClient Arguments class.
    /// </summary>
    public class TCPEventArgs : EventArgs
    {
        /// <summary>
        /// The Received data from the device we are connected to.
        /// </summary>
        public string RcvData { get; private set; }

         /// <summary>
        /// TCPEventArgs Constructor.
        /// </summary>
        /// <param name="rcvData"> The Received Data</param>
        public TCPEventArgs(string rcvData)
        {
            RcvData = rcvData;
        }
    }
}
