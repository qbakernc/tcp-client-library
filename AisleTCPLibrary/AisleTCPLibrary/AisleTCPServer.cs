﻿using System;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;

namespace AisleTCPLibrary
{
    /// <summary>
    /// AisleTCPServer class
    /// </summary>
    public class AisleTCPServer
    {
        /// <summary>
        /// DataReceivedEventHandler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public delegate void DataReceivedEventHandler(object sender, TCPEventArgs args);
        public event DataReceivedEventHandler OnDataReceived;

        private readonly TCPServer tcpServer;
        private readonly string addressToAccept = "0.0.0.0"; // Accepts Connection from any client.
        private string rcvString;                   // String received from the client.
        private readonly string delimiter;                   // Lets us know when we gathered all of the data

        /// <summary>
        /// Why am I able to push
        /// </summary>
        /// <param name="port"></param>
        /// <param name="buffSize"></param>
        /// <param name="maxConnections"></param>
        /// <param name="delimiter"></param>
        public AisleTCPServer(int port, int buffSize, int maxConnections, string delimiter)
        {
            this.delimiter = delimiter;

            tcpServer = new TCPServer(addressToAccept, port, buffSize, EthernetAdapterType.EthernetLANAdapter, maxConnections);
            tcpServer.SocketStatusChange += new TCPServerSocketStatusChangeEventHandler(Server_SocketStatusChange);
        }

        public void ListenForConnection()
        {
            SocketErrorCodes error = tcpServer.WaitForConnectionAsync(ServerConnectedCallback);
            CrestronConsole.PrintLine("WaitForConnectionAsync return: " + error);
        }

        private void ServerConnectedCallback(TCPServer myTCPServer, uint clientIndex)
        {
            if (clientIndex != 0)
            {
                CrestronConsole.PrintLine("Server listening on port #" + myTCPServer.PortNumber);
                CrestronConsole.PrintLine("Connected: client #" + clientIndex);

                /* Starts listeing for the specific client index. */
                myTCPServer.ReceiveDataAsync(clientIndex, ServerDataReceivedCallback);

                if (myTCPServer.MaxNumberOfClientSupported == myTCPServer.NumberOfClientsConnected)
                {
                    CrestronConsole.PrintLine("Client limit reached. Server will stop listening.");
                    myTCPServer.Stop();
                    CrestronConsole.PrintLine("Server State: " + myTCPServer.State);
                }
            }
            /* A clientIndex of 0 could mean that the server is no longer listening, or that the TLS handshake failed when a client tried to connect.
             * In the case of a TLS handshake failure, wait for another connection so that other clients can still connect. */
            else
            {
                CrestronConsole.Print("Error in ServerConnectedCallback: ");
                if ((myTCPServer.State & ServerState.SERVER_NOT_LISTENING) > 0)
                {
                    CrestronConsole.PrintLine("Server is no longer listening.");
                }
                else
                {
                    CrestronConsole.PrintLine("Unable to make connection with client.");
                    /* This connection failed, but keep waiting for another. */
                    myTCPServer.WaitForConnectionAsync(ServerConnectedCallback);
                }
            }

        }

        private void ServerDataReceivedCallback(TCPServer myTCPServer, uint clientIndex, int totalBytesRcv)
        {
            /* 0 or negative byte count indicates the connection has been closed. */
            if (totalBytesRcv <= 0)
            {
                CrestronConsole.PrintLine("Error: server's connection with client " + clientIndex + " has been closed.");
                myTCPServer.Disconnect(clientIndex);

                /* If server is not listening because of max number of clients, It will start listening again because a client disconnected. */
                if ((myTCPServer.State & ServerState.SERVER_NOT_LISTENING) > 0)
                    myTCPServer.WaitForConnectionAsync(ServerConnectedCallback);
            }
            else
            {
                byte[] rcvBytes = new byte[totalBytesRcv];
                Array.Copy(myTCPServer.GetIncomingDataBufferForSpecificClient(clientIndex), rcvBytes, totalBytesRcv);
                rcvString += Encoding.UTF8.GetString(rcvBytes, 0, totalBytesRcv);

                /* Use this block of code to see the data comming into the server on the CrestronConsole*/
                //CrestronConsole.PrintLine("----------- incoming message -----------\n\r" + rcvString + "\n\r---------- end of message ----------"); 

                /* Event will be called if the expected delimiter is found in the string. Clears the rcvString. */
                if ( rcvString.IndexOf(delimiter) >= 0)
                {
                    OnDataReceived?.Invoke(this, new TCPEventArgs(rcvString));
                    rcvString = "";
                }

                /* Begin waiting for another message from that same client. */
                myTCPServer.ReceiveDataAsync(clientIndex, ServerDataReceivedCallback);
            }
        }

        private void Server_SocketStatusChange(TCPServer myTCPServer, uint clientIndex, SocketStatus socketStatus)
        {
            /* Lets us know if the connection was succesfull or not. It also lets us know which specific client had a succesfull connection. */
            if (socketStatus == SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                CrestronConsole.PrintLine("Client #" + clientIndex + "connected");
            }
            else
            {
                CrestronConsole.PrintLine("Client #" + clientIndex + ": " + socketStatus + ".");
            }
        }
    }
}
