﻿using System;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;

namespace AisleTCPLibrary
{
    /// <summary>
    /// Aisle custom TCP Client class.
    /// </summary>
    public class AisleTCPClient
    {
        /// <summary>
        /// Data Received EventHandlers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"> Will hold the lamp hour value. </param>
        public delegate void DataReceivedEventHandler(object sender, TCPEventArgs args);

        /// <summary>
        /// Will trigger an event whenever the TCP Client receives data from the projector.
        /// </summary>
        public event DataReceivedEventHandler OnDataReceived;

        private readonly TCPClient tcpClient;
        private Byte[] sendBytes;    
        private string rcvString;     
        private readonly string delimiter;

        /// <summary>
        /// AisleTCPent Class Constructors.d
        /// </summary>
        /// <param name="IpAddress"> The IpAddress of the server. </param>
        /// <param name="port"> The port number of the server. </param>
        /// <param name="buffSize"> Buffer size. </param>
        /// <param name="delimiter"> The delimiter string will determine the end of the received data. </param>
        public AisleTCPClient(string IpAddress, int port, int buffSize, string delimiter)
        {
            this.delimiter = delimiter;

            tcpClient = new TCPClient(IpAddress, port, buffSize);
            tcpClient.SocketStatusChange += new TCPClientSocketStatusChangeEventHandler(MasterConfigList_SocketStatusChange);
        }

        /// <summary>
        /// Connects to the server and sends the data.
        /// </summary>
        /// <param name="sendBytes"> The bytes of data that will be sent to the server. </param>
        public void ConnectToServer(Byte[] sendBytes)
        {
            this.sendBytes = sendBytes;

            try
            {
                /* ClientConnectCallback is called once the client connects succesfully. */
                SocketErrorCodes error = tcpClient.ConnectToServerAsync(ClientConnectCallback);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("Error connecting to server: " + e.Message);
            }
        }

        private void ClientConnectCallback(TCPClient myTCPClient)
        {
            if (myTCPClient.ClientStatus == SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                /* Once connected, begin waiting for packets from the server.
                 * This call to ReceiveDataAsync is necessary to receive the FIN packet from the server in the event
                 * that the TLS handshake fails and the connection cannot be made. If you do not call ReceiveDataAsync here,
                 * the client will remain "connected" from its perspective, though no connection has been made. */
                myTCPClient.ReceiveDataAsync(ClientReceiveCallback);
                myTCPClient.SendData(sendBytes, sendBytes.Length);
            }
            else
            {
                CrestronConsole.PrintLine("clientConnectCallback: No connection could be made with the server.");
            }
        }

        private void ClientReceiveCallback(TCPClient myTCPClient, int totalBytesRcv)
        {
            /* 0 or negative byte count indicates the connection has been closed. */
            if (totalBytesRcv <= 0)
            {
                CrestronConsole.PrintLine("clientReceiveCallback: Could not receive message- connection closed");
            }
            else
            {
                try
                {
                    byte[] rcvBytes = new byte[totalBytesRcv];
                    Array.Copy(myTCPClient.IncomingDataBuffer, rcvBytes, totalBytesRcv);

                    /* Gathers the data into a string variable. */
                    rcvString += Encoding.UTF8.GetString(rcvBytes, 0, totalBytesRcv);

                    /* Event will be called if the delimiter is found in the string. Lets us know we gathered all of the data. */
                    if ( rcvString.IndexOf(delimiter) >= 0)
                    {
                        OnDataReceived?.Invoke(this, new TCPEventArgs(rcvString));
                        rcvString = "";
                        myTCPClient.DisconnectFromServer();
                    }
                    else
                    {
                        myTCPClient.ReceiveDataAsync(ClientReceiveCallback);
                    }
                }
                catch (Exception e)
                {
                    CrestronConsole.PrintLine("Exception in clientReceiveCallback: " + e.Message);
                }
            }
        }
        private void MasterConfigList_SocketStatusChange(TCPClient myTCPClient, SocketStatus clientSocketStatus)
        {
            CrestronConsole.PrintLine("Socket State: " + clientSocketStatus);
        }
    }
}
